package org.osb.fishing.gui;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import org.osb.fishing.Greyfish;
import org.osb.fishing.area.FishingArea;
import org.osb.fishing.area.FishingNode;
import org.osb.fishing.area.impl.Catherby;
import org.osb.fishing.session.SessionDetails;
import org.osbot.rs07.api.ui.Skill;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JCheckBox;

/**
 *
 * @author Dustin Greyfield
 */
@SuppressWarnings("serial")
public class SetupWindow extends JFrame {
	
	private FishingNode fishingNode = FishingNode.LOBSTER;

	/**
	 * Create the application.
	 */
	public SetupWindow(final Greyfish greyfish) {
		
		setBounds(100, 100, 212, 192);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JComboBox<String> fishingZoneSelector = new JComboBox<String>();
		fishingZoneSelector.setModel(new DefaultComboBoxModel<String>(new String[] {"Catherby"}));
		fishingZoneSelector.setSelectedIndex(0);
		fishingZoneSelector.setBounds(96, 11, 86, 20);
		getContentPane().add(fishingZoneSelector);
		
		JComboBox<FishingNode> fishingNodeSelector = new JComboBox<>();
		fishingNodeSelector.setModel(new DefaultComboBoxModel<FishingNode>(FishingNode.values()));
		fishingNodeSelector.setBounds(96, 42, 86, 20);
		getContentPane().add(fishingNodeSelector);
		fishingNode = FishingNode.LOBSTER;
		
		fishingNodeSelector.addActionListener(e -> fishingNode = (FishingNode) fishingNodeSelector.getSelectedItem());
		
		/*
		 * Builds and places the window labels.
		 */
		buildWindowLabels();
		
		final JButton btnNewButton = new JButton("Start");
		btnNewButton.addActionListener(e -> {
			
			if (greyfish.started == false) {
				this.setVisible(false);
				
				FishingArea fishingArea = null;
				
				switch (fishingZoneSelector.getSelectedItem().toString()) {
				
					case "Catherby":
						fishingArea = new Catherby(greyfish, fishingNode);
						break;
				
						default:
							fishingArea = new Catherby(greyfish, fishingNode);
							break;
				}
				
				greyfish.setFishingArea(fishingArea);
				greyfish.setSessionDetails(new SessionDetails(greyfish));
				greyfish.getSessionDetails().setStartExperience(greyfish.getSkills().getExperience(Skill.FISHING));
				greyfish.started = true;
				greyfish.getExperienceTracker().start(Skill.FISHING);
			}
			
		});

		btnNewButton.setBounds(10, 114, 176, 23);
		getContentPane().add(btnNewButton);
		
		/*
		 * Adds the checkbox for banking and the action listener.
		 */
		buildBankingCheckBox().addActionListener( e -> greyfish.changeUseBankFlag() );
		/*
		 * Adds the checkbox for antiban and the action listener.
		 */
		buildAntibanCheckBox().addActionListener( e -> greyfish.changeUseAntibanFlag() );
	}
	
	public void buildWindowLabels() {
		final JLabel lblFishingZone = new JLabel("Fishing Zone:");
		final JLabel lblFishType = new JLabel("Fish Type:");
		
		lblFishingZone.setBounds(10, 14, 76, 14);
		getContentPane().add(lblFishingZone);
		
		lblFishType.setBounds(10, 45, 64, 14);
		getContentPane().add(lblFishType);
	}
	
	/**
	 * Builds and constructs the {@code JCheckBox} for the banking option.
	 * 
	 * @return
	 * 		{@code JCheckBox}
	 */
	private JCheckBox buildBankingCheckBox() {
		final JCheckBox bankingCheckbox = new JCheckBox("Banking");
		
		bankingCheckbox.setBounds(96, 84, 97, 23);
		getContentPane().add(bankingCheckbox);
		
		return bankingCheckbox;
	}
	
	/**
	 * Builds and constructs the {@code JCheckBox} for the antiban option.
	 * 
	 * @return
	 * 		{@code JCheckBox}
	 */
	private JCheckBox buildAntibanCheckBox() {
		final JCheckBox antibanCheckbox = new JCheckBox("Antiban");

		antibanCheckbox.setBounds(6, 84, 97, 23);
		getContentPane().add(antibanCheckbox);
		
		return antibanCheckbox;
	}
}