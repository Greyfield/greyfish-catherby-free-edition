package org.osb.fishing;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.osb.fishing.antiban.AntibanFunction;
import org.osb.fishing.area.FishingArea;
import org.osb.fishing.area.FishingNode;
import org.osb.fishing.area.impl.Catherby;
import org.osb.fishing.gui.SetupWindow;
import org.osb.fishing.session.SessionDetails;
import org.osb.fishing.session.SessionState;
import org.osbot.rs07.api.ui.Message;
import org.osbot.rs07.api.ui.Message.MessageType;
import org.osbot.rs07.api.ui.Skill;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

/**
 * The core class that maintains the scripts functionality.
 * 
 * @author Dustin Greyfield
 */
@ScriptManifest(author = "Greyfield", info = "Supports all fishing nodes in the catherby zone with banking.", logo = "", name = "Greyfish (Catherby)", version = 1.1)

public class Greyfish extends Script {
	
	/**
	 * The {@link FishingArea} being used for this session.
	 */
	private FishingArea fishingArea = new Catherby(this, FishingNode.LOBSTER);

	/**
	 * The state flag for the current session.
	 */
	private SessionState sessionState = SessionState.WALK_TO_FISH;
	
	/**
	 * The {@link SessionDetails} instance chained.
	 */
	private SessionDetails sessionDetails;
	
	/**
	 * Flag for starting the script execution.
	 */
	public boolean started = false;
	
	/**
	 * Flag for whether or not the script should bank catches.
	 */
	private boolean useBank = false;
	
	/**
	 * Flag for whether or not the script should use the antiban.
	 */
	private boolean useAntiban = false;
	
	/**
	 * The {@link Font} instance to be used when painting.
	 */
	private final Font font = new Font("Comic Sans MS", Font.BOLD, 16);
	
	/**
	 * The {@link FishingNode} icon for the paint.
	 */
	private BufferedImage icon;
	
	@Override
	public void onMessage(final Message msg) {

		if (msg.getType() == MessageType.GAME) {
			
			if (msg.getMessage().contains("You catch")) {
				
				sessionDetails.incrementFishCaught();
			}
			
		}
	}
	
	@Override
	public void onStart() throws InterruptedException {
		new SetupWindow(this).setVisible(true);
		
		//TODO: THIS IS UGLY CODE
		try {
			icon = ImageIO.read(this.getClass().getResourceAsStream("/resources/lobster.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int onLoop() throws InterruptedException {
		
		if (started) {
			
			cycleAntiban();
			
			switch (sessionState) {
			
				case FIND_BANK:
					fishingArea.handleBankFinding();
					break;
					
				case BANKING:
					fishingArea.handleBanking();
					break;
					
				case FIND_FISH:
					fishingArea.handleFishFinding();
					break;
					
				case FISHING:
					fishingArea.handleFishing();
					break;
					
				case WALK_TO_BANK:
					
					if (!useBank) {
						
						getInventory().dropAll(fishingArea.getFishingNode().getItemIdentifiers());
						sessionState = SessionState.FIND_FISH;
						break;
					}
					
				case WALK_TO_FISH:
					fishingArea.handleWalking();
					break;

			}
		}

		return random(500, 1800);
	}
	
	@Override
	public void onPaint(Graphics2D graph) {
		
		if (started) {
			
			graph.setColor(new Color(0, 127, 255, 175));
			graph.fillRect(8, 345, 506, 129);
			
			graph.setColor(new Color(61, 52, 54));
			graph.setFont(font);
			graph.drawString("Session Time: " + sessionDetails.getAccumulatedTime(), 200, 385);
			graph.drawString("Fish Caught: " + sessionDetails.getFishCaught(), 215, 405);
			graph.drawString("Total Experience: " + getExperienceTracker().getGainedXP(Skill.FISHING), 230, 425);
			graph.drawString("Levels Gained: " + getExperienceTracker().getGainedLevels(Skill.FISHING), 245, 445);
			
			graph.drawImage(icon, null, 50, 375);
			drawPercentageBar(graph);
			drawMouse(graph);
		}
	}
	
	/**
	 * Paints the level progress bar.
	 * 
	 * @param graph
	 */
	private void drawPercentageBar(final Graphics2D graph) {
		final double goal = ((getSkills().getExperienceForLevel(getSkills().getStatic(Skill.FISHING) + 1) - (getSkills().getExperienceForLevel(getSkills().getStatic(Skill.FISHING)))));
		final double remainder =  getSkills().experienceToLevel(Skill.FISHING);
		final double percentage = ((goal - remainder) / goal);
		final int fillAmount = (int) (percentage * 506);

		graph.setColor(new Color(217, 236, 255));
		graph.setFont(graph.getFont().deriveFont(14.0f));
		graph.drawString("Percentage to level " + (getSkills().getStatic(Skill.FISHING) + 1) + ": ("+ ((int)(percentage * 100)) +"%) " + ((int)(goal - remainder)) + " / " + ((int)goal) + " | " + getExperienceTracker().getGainedXPPerHour(Skill.FISHING) + "/hour", 10, 315);
		
		graph.setColor(new Color(128, 85, 0, 200));
		graph.drawRect(7, 319, 507, 11);
		graph.setColor(new Color(0, 127, 255, 175));
		graph.fillRect(8, 320, 506, 10);
			
		graph.setColor(new Color(127, 0, 255, 175));
		graph.fillRect(8, 320, fillAmount, 10);
	}
	
	private void drawMouse(final Graphics2D graph) {

		graph.setColor(new Color(127, 0, 255, 70));
		graph.fillOval((int)getMouse().getPosition().getX() - 15, (int)getMouse().getPosition().getY() - 15, 30, 30);
		graph.setColor(new Color(255, 0, 0));
		graph.fillOval((int)getMouse().getPosition().getX() - 3, (int)getMouse().getPosition().getY() - 3, 6, 6);
	}
	
	/**
	 * Decouples the {@code #onLoop()} method by splitting antiban functions
	 * from the main script logic.
	 */
	private void cycleAntiban() throws InterruptedException {
		
		if (random(3) < 1)
			AntibanFunction.MOVE_MOUSE_RANDOMLY.execute(this);
		
		if (random(10) < 1)
			AntibanFunction.MOVE_SCREEN_RANDOMLY.execute(this);
		
		if (useAntiban && random(25) < 1 && sessionState == SessionState.FISHING) {
			AntibanFunction.selectRandom().execute(this);
			sleep(random(400, 700));
		}
	}
	
	/**
	 * Gets the {@code SessionState}.
	 * 
	 * @return
	 * 		{@link #sessionState}
	 */
	public SessionState getSessionState() {
		
		return sessionState;
	}
	
	/**
	 * Sets the {@code SessionState} flag.
	 * 
	 * @param sessionState
	 * 		The {@code SessionState} value.
	 */
	public void setSessionState(final SessionState sessionState) {
		
		this.sessionState = sessionState;
	}
	
	/**
	 * Gets the {@link SessionDetails} instance chained to this bot.
	 * 
	 * @return
	 * 		{@link #sessionDetails}
	 */
	public SessionDetails getSessionDetails() {
		
		return sessionDetails;
	}
	
	/**
	 * Sets the {@code SessionDetails} chain.
	 * 
	 * @param sessionDetails
	 */
	public void setSessionDetails(final SessionDetails sessionDetails) {
		this.sessionDetails = sessionDetails;
	}
	
	/**
	 * Sets the fishing area zone.
	 * 
	 * @param fishingArea
	 */
	public void setFishingArea(final FishingArea fishingArea) {
		
		this.fishingArea = fishingArea;
	}
	
	/**
	 * Reverses the {@code #useBank} flag based on the current state.
	 * 
	 * @return
	 * 		{@code #useBank}
	 */
	public boolean changeUseBankFlag() {
		
		return (!useBank ? (useBank = true) : (useBank = false));
	}
	
	/**
	 * Reverses the {@code #useBank} flag based on the current state.
	 * 
	 * @return
	 * 		{@code #useBank}
	 */
	public boolean changeUseAntibanFlag() {
		
		return (!useAntiban ? (useAntiban = true) : (useAntiban = false));
	}
	
	/**
	 * Gets the {@code #useBank} flag.
	 * 
	 * @return
	 * 		{@link #useBank}
	 */
	public boolean useBank() {
		
		return useBank;
	}
	
	/**
	 * Gets the {@code #useAntiban} flag.
	 * 
	 * @return
	 * 		{@link #useAntiban}
	 */
	public boolean useAntiban() {
		
		return useAntiban;
	}
}