package org.osb.fishing.session;

/**
 *
 * @author Dustin Greyfield
 */
public enum SessionState {
	
	WALK_TO_BANK,
	
	FIND_BANK,
	
	BANKING,
	
	WALK_TO_FISH,
	
	FIND_FISH,
	
	FISHING

}
