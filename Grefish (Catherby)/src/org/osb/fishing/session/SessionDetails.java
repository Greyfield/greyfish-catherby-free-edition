package org.osb.fishing.session;

import java.util.concurrent.TimeUnit;

import org.osb.fishing.Greyfish;
import org.osbot.rs07.api.ui.Skill;

/**
 *
 * @author Dustin Greyfield
 */
public class SessionDetails {
	
	/**
	 * The script instance chained to this session.
	 */
	private final Greyfish greyfish;
	
	/**
	 * The system time when the bot session began.
	 */
	private final long startTime;
	
	/**
	 * Experience had at the start of the bot session.
	 */
	private int startExperience;
	
	/**
	 * The total amount of fish banked.
	 */
	private int fishCaught = 0;
	
	/**
	 * Constructs a {@code SessionDetails} instance chained to a bot.
	 * 
	 * @param script
	 * 		The chained script.
	 */
	public SessionDetails(final Greyfish catherbyFisher) {
		this.greyfish = catherbyFisher;
		startTime = System.currentTimeMillis();
	}

	/**
	 * Gets the accumulated session time.
	 * 
	 * @return
	 * 		Formated session time.
	 */
	public String getAccumulatedTime() {
		final long cachedTime = (System.currentTimeMillis() - startTime);
		
		return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(cachedTime), (TimeUnit.MILLISECONDS.toMinutes(cachedTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(cachedTime))), (TimeUnit.MILLISECONDS.toSeconds(cachedTime) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(cachedTime))));
	}
	
	/**
	 * Sets the {@code #startExperience} variab
	 * @param startExperience
	 */
	public void setStartExperience(final int startExperience) {
		
		this.startExperience = startExperience;
	}
	
	/**
	 * Gets the sessions accumulated experience gain.
	 * 
	 * @return
	 * 		The accumulated experience.
	 */
	public int getAccumulatedExperience() {
		
		return (greyfish.getSkills().getExperience(Skill.FISHING) - startExperience);
	}
	
	/**
	 * Gets the {@link #fishCaught} of fish.
	 * 
	 * @return
	 * 		{@link #fishCaught}
	 */
	public int getFishCaught() {
		
		return fishCaught;
	}
	
	/**
	 * Increments the amount of fish banked by a set value.
	 */
	public void incrementFishCaught() {
		
		fishCaught++;
	}
}
