package org.osb.fishing.area;

import org.osb.fishing.Greyfish;
import org.osb.fishing.antiban.AntibanFunction;
import org.osb.fishing.session.SessionState;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.script.MethodProvider;

import java.util.EnumSet;
import java.util.Optional;;

/**
 * Represents an area available for fishing.
 * 
 * @author Dustin Greyfield
 */
public abstract class FishingArea {
	
	/**
	 * Gets the array of {@link Position}'s to follow when pathing
	 * to the fishing location.
	 * 
	 * @return
	 * 		The fishing location path.
	 */
	public abstract Position[] getFishingSpotPath();
	
	/**
	 * Gets the array of {@link Position}'s to follow when pathing
	 * to the bank location.
	 * 
	 * @return
	 * 		The banking location.
	 */
	public abstract Position[] getBankPath();
	
	/**
	 * Gets the array of integers representing the {@code FishingArea} bank booths.
	 * 
	 * @return
	 * 		The banking identifiers.
	 */
	public abstract int[] getBankBoothIdentifiers();
	
	/**
	 * A {@link EnumSet} implementation holding the available {@code FishingNode}
	 * values for the {@code FishingArea} instance.
	 * 
	 * @return
	 * 		The available {@link FishingNode}'s.
	 */
	public abstract EnumSet<FishingNode> getFishingNodes();
	
	/**
	 * The script instance chained.
	 */
	private final Greyfish greyfish;
	
	/**
	 * The selected fishing node.
	 */
	private final FishingNode fishingNode;
	
	/**
	 * Constructs a FishingArea instance.
	 * 
	 * @param greyfish
	 * 		The bot instance to chain.
	 * @param fishingNode
	 * 		The type of fishing node.
	 */
	public FishingArea(final Greyfish greyfish, final FishingNode fishingNode) {
		this.greyfish = greyfish;
		this.fishingNode = fishingNode;
	}
	
	/**
	 * Handles locating and interacting with a bank booth.
	 * 
	 * @throws InterruptedException
	 */
	public void handleBankFinding() throws InterruptedException {
		final Optional<RS2Object> interactingObject = Optional.ofNullable(greyfish.getObjects().closest(getBankBoothIdentifiers()));
		
		if (interactingObject.isPresent()) {
			interactingObject.get().interact("Bank");
			//interactingObject = null;
			greyfish.setSessionState(SessionState.BANKING);
			Greyfish.sleep(MethodProvider.random(2000, 3000));
			return;
		}
		
		AntibanFunction.MOVE_SCREEN_RANDOMLY.execute(greyfish);
	}
	
	/**
	 * Handles the banking of the raw fish.
	 * 
	 * @throws InterruptedException
	 */
	public void handleBanking() throws InterruptedException {
		if (greyfish.getBank().isOpen()) {
			greyfish.getBank().depositAll(fishingNode.getItemIdentifiers());
			greyfish.setSessionState(SessionState.WALK_TO_FISH);
			Greyfish.sleep(MethodProvider.random(1000, 2000));
		} else {
			greyfish.setSessionState(SessionState.FIND_BANK);
		}
	}
	
	/**
	 * Handles walking to and from two different paths. I.e 
	 * dependent upon {@link #sessionState} value.
	 */
	public void handleWalking() {
		final Position[] currentPath = (greyfish.getSessionState() == SessionState.WALK_TO_BANK) ? getBankPath() : getFishingSpotPath();
		
		if (greyfish.myPosition().distance(currentPath[currentPath.length - 1]) > 4) {
			
			greyfish.getWalking().webWalk(currentPath);
			return;
		}
		
		greyfish.setSessionState(greyfish.getSessionState() == SessionState.WALK_TO_BANK ? SessionState.FIND_BANK : SessionState.FIND_FISH);
	}
	
	/**
	 * Handles the finding of a fishing node and interacting
	 * with it.
	 * @throws InterruptedException 
	 */
	public void handleFishFinding() throws InterruptedException {
		final Optional<NPC>interactingNPC = Optional.ofNullable(greyfish.getNpcs().closest(fishingNode.getSpotIdentifier()));
		
		if (interactingNPC.isPresent()) {
			interactingNPC.get().interact(fishingNode.getOption());
			greyfish.setSessionState(SessionState.FISHING);
			Greyfish.sleep(MethodProvider.random(1000, 3000));
			
			return;
		}
		
		greyfish.setSessionState(SessionState.WALK_TO_FISH);
	}
	
	/**
	 * Handles the {@link SessionState#FISHING} value.
	 */
	public void handleFishing() {
		if (greyfish.getInventory().isFull()) {
			greyfish.setSessionState(SessionState.WALK_TO_BANK);
			return;
		}
		
		if (!greyfish.myPlayer().isAnimating()) {
			greyfish.setSessionState(SessionState.FIND_FISH);
		}
	}
	
	/**
	 * Gets the active {@link FishingNode} type.
	 * 
	 * @return
	 * 		{@code #fishingNode}
	 */
	public FishingNode getFishingNode() {
		
		return fishingNode;
	}

}