package org.osb.fishing.area.impl;

import java.util.EnumSet;

import org.osb.fishing.Greyfish;
import org.osb.fishing.area.FishingArea;
import org.osb.fishing.area.FishingNode;
import org.osbot.rs07.api.map.Position;

/**
 * The fishing zone for Catherby.
 * 
 * @author Dustin Greyfield
 */
public class Catherby extends FishingArea {

	/**
	 * The fishing path.
	 */
	private static final Position[] FISHING_PATH = {
			new Position(2809, 3438, 0),
			new Position(2815, 3436, 0),
			new Position(2823, 3437, 0),
			new Position(2833, 3436, 0),
			new Position(2839, 3433, 0),
			new Position(2847, 3433, 0),
	};

	/**
	 * The banking path.
	 */
	private static final Position[] BANK_PATH = {
			new Position(2851, 3431, 0),
			new Position(2847, 3433, 0),
			new Position(2839, 3433, 0),
			new Position(2833, 3436, 0),
			new Position(2823, 3437, 0),
			new Position(2815, 3436, 0),
			new Position(2809, 3435, 0)
	};
	
	/**
	 * The areas bank booth ids.
	 */
	private final int[] bankBoothIdentifiers = { 11744, 27249 };
	
	/**
	 * The {@link EnumSet} of {@code FishingNode} values available to this area.
	 */
	private final EnumSet<FishingNode> fishingNodes = EnumSet.of(FishingNode.LOBSTER);
	
	/**
	 * Constructs the Catherby fishing area.
	 * 
	 * @param greyfish
	 * 		The {@link Script} instance.
	 * @param fishingNode
	 * 		The fishing node to fish.
	 */
	public Catherby(final Greyfish greyfish, final FishingNode fishingNode) {
		super(greyfish, fishingNode);
	}

	@Override
	public Position[] getFishingSpotPath() {
		
		return FISHING_PATH;
	}

	@Override
	public Position[] getBankPath() {
		
		return BANK_PATH;
	}
	
	@Override
	public int[] getBankBoothIdentifiers() {
		
		return bankBoothIdentifiers;
	}
	
	@Override
	public EnumSet<FishingNode> getFishingNodes() {
		
		return fishingNodes;
	}
}