package org.osb.fishing.area;

/**
 *
 * @author Dustin Greyfield
 */
public enum FishingNode {
	
	LOBSTER(1519, "Cage", 377),
	
	BIG_NET(1520, "Net", 353, 341, 363, 1061, 1059, 407, 401),
	
	SWORDFISH_T(1519, "Harpoon", 371, 359),
	
	SHARK(1520, "Harpoon", 383);
	
	/**
	 * The item identifiers for the raw fish.
	 */
	private final int[] itemIdentifiers;
	
	/**
	 * The identifier of the fishing NPC.
	 */
	private final int spotIdentifier;
	
	/**
	 * The node option.
	 */
	private final String option;

	/**
	 * Default enumerated type constructor.
	 * 
	 * @param spotIdentifiers
	 * 		The identifiers of the fishing NPC.
	 */
	private FishingNode(final int spotIdentifier, final String option, final int... itemIdentifiers) {
		this.itemIdentifiers = itemIdentifiers;
		this.spotIdentifier = spotIdentifier;
		this.option = option;
	}
	
	/**
	 * Gets the {@code FishingNode} item identifier.
	 * 
	 * @return
	 * 		{@link #itemIdentifiers}
	 */
	public int[] getItemIdentifiers() {
		
		return itemIdentifiers;
	}
	
	/**
	 * Gets the integer identifiers of the fishing spot.
	 * 
	 * @return
	 * 		{@code #spotIdentifiers}.
	 */
	public int getSpotIdentifier() {
		
		return spotIdentifier;
	}
	
	/**
	 * Gets the node option.
	 * 
	 * @return
	 */
	public String getOption() {
		
		return option;
	}
}