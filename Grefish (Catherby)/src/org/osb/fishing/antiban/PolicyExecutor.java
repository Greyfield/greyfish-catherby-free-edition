package org.osb.fishing.antiban;

import org.osb.fishing.Greyfish;

/**
 * Functional interface used for executing an anti-ban task.
 * 
 * @author Dustin Greyfield
 */
public interface PolicyExecutor {

	/**
	 * Executes the anti-ban function.
	 * @throws InterruptedException 
	 */
	public void execute(final Greyfish greyfish) throws InterruptedException;
	
}