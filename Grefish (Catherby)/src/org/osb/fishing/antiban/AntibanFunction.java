package org.osb.fishing.antiban;

import org.osb.fishing.Greyfish;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.input.mouse.EntityDestination;
import org.osbot.rs07.script.MethodProvider;

import static org.osbot.rs07.script.MethodProvider.random;

import java.util.Collections;
import java.util.List;

/**
 * Enumerated type holding Antiban pattern functions.
 * 
 * @author Dustin Greyfield
 */
public enum AntibanFunction implements PolicyExecutor {

	SELECT_RANDOM_TAB {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			greyfish.getTabs().open(Tab.values()[random(Tab.values().length)]);
			Greyfish.sleep(random(600, 1000));
			
		}
	},
	
	SELECT_SKILL_TAB_HOVER {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			greyfish.getTabs().open(Tab.SKILLS);
			Greyfish.sleep(random(1200, 2000));
			greyfish.getWidgets().get(320, SkillWidget.getRandomChildId());
			
		}
	},
	
	SELECT_MAGIC_TAB_HOVER {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			greyfish.getTabs().open(Tab.MAGIC);
			Greyfish.sleep(random(1200, 2000));
			greyfish.getWidgets().get(218, MethodProvider.random(64)).hover();
			
		}
	}, 
	
	TOGGLE_EXP_COUNTER {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			if (!greyfish.getWidgets().interact(160, 1, "Show"))
				greyfish.getWidgets().interact(160, 1, "Hide");
				
		}
	},
	
	EXAMINE_RANDOM_NPC {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			if (!greyfish.getNpcs().getAll().isEmpty()) {
				final List<NPC> arrayCopy = greyfish.getNpcs().getAll();
				Collections.shuffle(arrayCopy);
				
				for (final NPC npc : arrayCopy) {

					if (npc.getPosition().distance(greyfish.myPosition()) > 4)
						continue;
					
					npc.examine();
					return;
				}
			}
		}
	},
	
	MOUSE_RANDOM_NPC {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			if (!greyfish.getNpcs().getAll().isEmpty()) {
				final List<NPC> arrayCopy = greyfish.getNpcs().getAll();
				Collections.shuffle(arrayCopy);
				
				for (final NPC npc : arrayCopy) {

					if (npc.getPosition().distance(greyfish.myPosition()) > 4)
						continue;
					
					final EntityDestination dest = new EntityDestination(greyfish.getBot(), arrayCopy.get(0));
					greyfish.getMouse().move(dest);
					return;
				}
			}
		}
	},
	
	MOVE_MOUSE_RANDOMLY {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			greyfish.getMouse().moveRandomly();
			
		}
	},
	
	EXAMINE_RANDOM_OBJECT {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			if (!greyfish.getObjects().getAll().isEmpty()) {
				final List<RS2Object> arrayCopy = greyfish.getObjects().getAll();
				Collections.shuffle(arrayCopy);
				
				for (final RS2Object obj : arrayCopy) {

					if (obj.getPosition().distance(greyfish.myPosition()) > 4)
						continue;
					
					obj.examine();
					return;
				}
			}
		}
	},
	
	MOUSE_RANDOM_OBJECT {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			if (!greyfish.getObjects().getAll().isEmpty()) {
				final List<RS2Object> arrayCopy = greyfish.getObjects().getAll();
				Collections.shuffle(arrayCopy);
				
				for (final RS2Object obj : arrayCopy) {

					if (obj.getPosition().distance(greyfish.myPosition()) > 4)
						continue;
					
					final EntityDestination dest = new EntityDestination(greyfish.getBot(), arrayCopy.get(0));
					greyfish.getMouse().move(dest);
					return;
				}
			}
		}
	},
	
	ROTATE_SCREEN_YEW_HALF {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			greyfish.getCamera().moveYaw(MethodProvider.random(15, 160));
			
		}
	},
	
	ROTATE_SCREEN_YEW_RANDOM {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			greyfish.getCamera().moveYaw(MethodProvider.random(15, 320));
			
		}
	},
	
	ROTATE_SCREEN_YEW_LARGE {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			greyfish.getCamera().moveYaw(MethodProvider.random(120, 320));
			
		}
	},
	
	CHANGE_SCREEN_PITCH_HALF {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			greyfish.getCamera().movePitch(random(15, 36));
			
		}
	},
	
	CHANGE_SCREEN_PITCH_LARGE {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			greyfish.getCamera().movePitch(MethodProvider.random(30, 67));
			
		}
	},
	
	CHANGE_SCREEN_PITCH_RANDOM {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			greyfish.getCamera().movePitch(MethodProvider.random(5, 67));
			
		}
	},
	
	MOVE_SCREEN_RANDOMLY {
		@Override
		public void execute(Greyfish greyfish) throws InterruptedException {
			
			ROTATE_SCREEN_YEW_RANDOM.execute(greyfish);
			Greyfish.sleep(random(500, 1200));
			CHANGE_SCREEN_PITCH_RANDOM.execute(greyfish);
			
		}
	};
	
	/**
	 * Enumerated type constructor.
	 * 
	 * @param policyExecutor
	 * 		The {@code PolicyExecutor} instance.
	 */
	private AntibanFunction() { }
	
	/**
	 * Selects a random function based on the size of the enumeration.
	 * 
	 * @return
	 * 		{@code AntibanFunction}.
	 */
	public static AntibanFunction selectRandom() {
		
		return values()[random(values().length)];
	}
}