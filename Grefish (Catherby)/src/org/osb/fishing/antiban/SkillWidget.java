package org.osb.fishing.antiban;

import org.osbot.rs07.script.MethodProvider;

/**
 * A generic enumeration type containing skills.
 * This will be added onto later.
 * 
 * @author Dustin Greyfield
 */
public enum SkillWidget {
	
	ATTACK,  
	
	DEFENCE,
	
	STRENGTH, 
	
	HITPOINTS,
	
	RANGE, 
	
	PRAYER,
	
	MAGIC,
	
	COOKING, 
	
	WOODCUTTING, 
	
	FLETCHING, 
	
	FISHING, 
	
	FIREMAKING, 
	
	CRAFTING,
	
	SMITHING,
	
	MINING, 
	
	HERBLORE, 
	
	AGILITY, 
	
	THIEVING, 
	
	SLAYER, 
	
	FARMING,
	
	RUNECRAFTING, 
	
	HUNTER, 
	
	CONSTRUCTION;
	
	public static int getRandomChildId() {
		
		return (values()[MethodProvider.random(values().length)].ordinal());
	}	
}